package com.demo.Accounting.systems;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;

import com.demo.Accounting.systems.repository.ExpenseRepository;
import com.demo.Accounting.systems.repository.MerchantRepository;
import com.demo.Accounting.systems.repository.SyncEventsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import okhttp3.OkHttpClient;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class AccountingSystemsApplication implements CommandLineRunner {

    @Autowired
    private ObjectMapper objectMapper;

    public static void main(String[] args) {
        SpringApplication.run(AccountingSystemsApplication.class, args);
    }

    @Bean
    public OkHttpClient getClient() {
        return new OkHttpClient().newBuilder().connectTimeout(3000, TimeUnit.MILLISECONDS)
                .build();
    }


    @Autowired
    ExpenseRepository expenseRepository;

    @Autowired
    SyncEventsRepository syncEventsRepository;

    @Autowired
    MerchantRepository merchantRepository;

    @PostConstruct
    public void setUp() {
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Override
    public void run(String... args) throws Exception {
        expenseRepository.deleteAll();
        syncEventsRepository.deleteAll();
        merchantRepository.deleteAll();
        System.err.println("deleting present data");
    }
}