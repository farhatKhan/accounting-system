package com.demo.Accounting.systems.controller;

import com.demo.Accounting.systems.exception.CustomException;
import com.demo.Accounting.systems.service.ExpensifyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
@Slf4j
public class SyncController {

    @Autowired
    ExpensifyService expensifyService;

    @GetMapping("/sync-vendors")
    public ResponseEntity<? extends Object> syncVendors() {
        try {
            return expensifyService.getExpenses() ? ResponseEntity.ok().body("Vendor info Sync successfully") : null;
        } catch (Exception exception) {
            log.error("Exception in vendor sync, vendor sync failed", exception);
            throw new CustomException("Exception while vendor sync : ", exception.getMessage());
        }
    }

}
