package com.demo.Accounting.systems.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpensifyDto {

    public Long reportNum;
    public String merchant;
    public Long amount;
    public String expenseCategory;
    public Long transactionID;
    public LocalDate transactionDate;

    public ExpensifyDto(String merchant, Long amount, String expenseCategory, Long transactionID, LocalDate transactionDate) {
        this.merchant = merchant;
        this.amount = amount;
        this.expenseCategory = expenseCategory;
        this.transactionID = transactionID;
        this.transactionDate = transactionDate;
    }
}
