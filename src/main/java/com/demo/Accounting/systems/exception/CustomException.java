package com.demo.Accounting.systems.exception;

/**
 * custom exception.
 */
public class CustomException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String message = "Something Went Wrong, please try later";
    private String description;

    public CustomException(String message) {
        this.message = message;
    }

    public CustomException(String message, String description) {
        if (null != message && !message.isEmpty())
            this.message = message;
        this.description = description;
    }

    public CustomException(Throwable cause, String message) {
        super(cause);
        this.message = message;
    }

    public CustomException(Throwable cause, String message, String description) {
        super(cause);
        this.message = message;
        this.description = description;
    }

    /**
     * Overrides RuntimeException's public String getMessage().
     *
     * @return a localized string description
     */
    @Override
    public String getMessage() {
        return this.message;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CustomException [message=" + message + ", description=" + description + "]";
    }

}
