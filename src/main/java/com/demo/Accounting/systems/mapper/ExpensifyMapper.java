package com.demo.Accounting.systems.mapper;

import com.demo.Accounting.systems.dto.ExpensifyDto;
import com.demo.Accounting.systems.model.Expense;
import com.demo.Accounting.systems.model.Merchant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import static org.mapstruct.NullValueMappingStrategy.RETURN_NULL;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_NULL)
public abstract class ExpensifyMapper {

    /**
     * map ExpensifyDto to Merchant
     *
     * @param expensifyDto
     * @return Merchant
     */
    @Mappings({@Mapping(source = "merchant", target = "name")})
    public abstract Merchant toMerchant(ExpensifyDto expensifyDto);

    /**
     * map ExpensifyDto to Merchant
     *
     * @param expensifyDto
     * @return Merchant
     */
    @Mappings({@Mapping(source = "transactionID", target = "transactionId")})
    public abstract Expense toExpense(ExpensifyDto expensifyDto);

}
