package com.demo.Accounting.systems.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Credentials model.
 */
@Data
@AllArgsConstructor
public class Credentials {
    private String partnerUserID;
    private String partnerUserSecret;
}
