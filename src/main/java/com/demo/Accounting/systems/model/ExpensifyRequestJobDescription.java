package com.demo.Accounting.systems.model;

import lombok.Data;

/**
 * ExpensifyRequestJobDescription model.
 */
@Data
public class ExpensifyRequestJobDescription {
    private String type;
    private Credentials credentials;
    private OnReceive onReceive;
    private InputSettings inputSettings;
    private OutputSettings outputSettings;
    private String fileName;
    private String fileSystem;
}
