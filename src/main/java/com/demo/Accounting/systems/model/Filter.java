package com.demo.Accounting.systems.model;

import lombok.Data;

/**
 * Filter model.
 */
@Data
public class Filter {
    private String reportIDList;
    private String startDate;
    private String endDate;
}
