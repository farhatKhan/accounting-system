package com.demo.Accounting.systems.model;

import lombok.Data;

/**
 * InputSettings model.
 */
@Data
public class InputSettings {
    private String type;
    private Filter filters;
    private Long limit;
}
