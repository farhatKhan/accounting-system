package com.demo.Accounting.systems.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Merchant collection.
 */
@Document(collection = "merchant")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class Merchant {

    @Id
    private String id;

    @NotBlank
    @Size(max = 100)
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
