package com.demo.Accounting.systems.model;

import lombok.Data;

import java.util.List;

/**
 * OnReceive model.
 */
@Data
public class OnReceive {
    private List<String> immediateResponse;
}
