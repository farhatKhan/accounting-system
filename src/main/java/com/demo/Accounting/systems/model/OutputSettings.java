package com.demo.Accounting.systems.model;

import lombok.Data;

/**
 * OutputSettings model.
 */
@Data
public class OutputSettings {
    private String fileExtension;
}
