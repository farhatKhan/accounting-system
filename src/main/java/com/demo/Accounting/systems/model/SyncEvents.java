package com.demo.Accounting.systems.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import java.time.LocalDate;

/**
 * SyncEvents collection.
 */
@Document(collection = "sync_events")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class SyncEvents {

    @Id
    private String id;

    @Column(name = "last_sync_date")
    private LocalDate lastSyncDate = LocalDate.now();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(LocalDate lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }
}
