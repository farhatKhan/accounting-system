package com.demo.Accounting.systems.repository;

import com.demo.Accounting.systems.model.Expense;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Expense Repository.
 */
@Repository
public interface ExpenseRepository extends MongoRepository<Expense, String> {
}
