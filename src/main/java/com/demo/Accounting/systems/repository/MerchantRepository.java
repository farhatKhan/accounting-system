package com.demo.Accounting.systems.repository;

import com.demo.Accounting.systems.model.Merchant;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Merchant Repository.
 */
@Repository
public interface MerchantRepository extends MongoRepository<Merchant, String> {

    public Boolean existsByName(String name);

    public Merchant findByName(String merchant);
}
