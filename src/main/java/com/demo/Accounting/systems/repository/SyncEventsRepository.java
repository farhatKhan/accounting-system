package com.demo.Accounting.systems.repository;

import com.demo.Accounting.systems.model.SyncEvents;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Sync Event Repository.
 */
@Repository
public interface SyncEventsRepository extends MongoRepository<SyncEvents, String> {

    /**
     * Get first sync event sort by last sync date.
     *
     * @return SyncEvents.
     */
    public SyncEvents findFirstByOrderByLastSyncDateDesc();
}
