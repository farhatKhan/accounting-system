package com.demo.Accounting.systems.service;


import com.demo.Accounting.systems.dto.ExpensifyDto;
import com.demo.Accounting.systems.mapper.ExpensifyMapper;
import com.demo.Accounting.systems.model.Credentials;
import com.demo.Accounting.systems.model.*;
import com.demo.Accounting.systems.repository.ExpenseRepository;
import com.demo.Accounting.systems.repository.MerchantRepository;
import com.demo.Accounting.systems.repository.SyncEventsRepository;
import com.demo.Accounting.systems.util.ExpensifyUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ExpensifyService {

    @Value("${expensify.api.url}")
    private String url;
    @Value("${expensify.api.user.id}")
    private String partnerUserID;
    @Value("${expensify.api.secret.key}")
    private String partnerUserSecret;
    @Value("${expensify.inputSettings.type}")
    private String inputSettingsType;
    @Value("${expensify.inputSettings.filter.reportIDList}")
    private String filterReportIDList;
    @Value("${expensify.outputSettings.fileExtension}")
    private String outputFileExtension;
    @Value("${expensify.inputSettings.limit}")
    private String limit;

    @Autowired
    OkHttpClient okHttpClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    ExpenseRepository expenseRepository;

    @Autowired
    SyncEventsRepository syncEventsRepository;

    @Autowired
    MerchantRepository merchantRepository;

    @Autowired
    ExpensifyMapper expensifyMapper;

    @Autowired
    MongoTemplate mongoTemplate;

    /**
     * Get request builder.
     *
     * @param body
     * @return Request.
     */
    public Request getRequestBuilder(RequestBody body) {
        return new Request.Builder()
                .url(url + "/Integration-Server/ExpensifyIntegrations")
                .method("POST", body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
    }

    /**
     * create basic request job description for expensify api call.
     *
     * @param requestType
     * @return ExpensifyRequestJobDescription.
     */
    public ExpensifyRequestJobDescription getRequestJobDescription(String requestType) {
        ExpensifyRequestJobDescription requestJobDescription = new ExpensifyRequestJobDescription();
        requestJobDescription.setType(requestType);
        requestJobDescription.setCredentials(new Credentials(partnerUserID, partnerUserSecret));
        return requestJobDescription;
    }

    /**
     * create request body for file type expensify api call.
     *
     * @return RequestBody.
     */
    public RequestBody getFileRequestBody() {
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        ExpensifyRequestJobDescription requestJobDescription = getRequestJobDescription("file");
        requestJobDescription.setInputSettings(getInputSettings());
        requestJobDescription.setOutputSettings(getOutputSettings());
        requestJobDescription.setOnReceive(getOnReceiveSettings());
        String json = convertToJsonObject(requestJobDescription) + ExpensifyUtil.readTemplateFile();
        return RequestBody.create(json, mediaType);
    }

    /**
     * create request body for download  type expensify api call.
     *
     * @param fileName
     * @return RequestBody.
     */
    public RequestBody getDownloadRequestBody(String fileName) {
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        ExpensifyRequestJobDescription requestJobDescription = getRequestJobDescription("download");
        requestJobDescription.setFileName(fileName);
        requestJobDescription.setFileSystem("integrationServer");
        return RequestBody.create((String) convertToJsonObject(requestJobDescription), mediaType);
    }

    /**
     * create InputSettings object for request job description of expensify api call.
     *
     * @return InputSettings.
     */
    public InputSettings getInputSettings() {
        InputSettings inputSettings = new InputSettings();
        inputSettings.setType(inputSettingsType);
        inputSettings.setFilters(getFilter());
        inputSettings.setLimit(Long.parseLong(limit));
        return inputSettings;
    }

    /**
     * create filter object for input setting of request job description.
     *
     * @return Filter.
     */
    public Filter getFilter() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
        Filter filter = new Filter();
//        filter.setReportIDList(filterReportIDList);
        SyncEvents syncEvents = syncEventsRepository.findFirstByOrderByLastSyncDateDesc();
        if (syncEvents != null) {
            filter.setStartDate(LocalDate.parse(syncEvents.getLastSyncDate().toString()).format(formatter));
        } else {
            filter.setStartDate("2016-01-01");
        }
        filter.setEndDate(LocalDate.now().format(formatter));
        return filter;
    }

    /**
     * create OutputSettings object for request job description.
     *
     * @return OutputSettings.
     */
    public OutputSettings getOutputSettings() {
        OutputSettings outputSettings = new OutputSettings();
        outputSettings.setFileExtension(outputFileExtension);
        return outputSettings;
    }

    /**
     * create OnReceive object for request job description.
     *
     * @return OnReceive.
     */
    public OnReceive getOnReceiveSettings() {
        OnReceive onReceive = new OnReceive();
        onReceive.setImmediateResponse(Arrays.asList("returnRandomFileName"));
        return onReceive;
    }

    /**
     * convert the java object into json.
     *
     * @param object
     * @return String.
     */
    private String convertToJsonObject(Object object) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "requestJobDescription=" + json;
    }

    /**
     * pull the vendor and its expenses info from expensify.
     *
     * @return Boolean.
     */
    public Boolean getExpenses() {
        Boolean status = Boolean.TRUE;
        try {
            Request fileRequests = getRequestBuilder(getFileRequestBody());
            Response fileResponses = okHttpClient.newCall(fileRequests).execute();
            String fileName;
            fileName = fileResponses.body().string();
            Request request = getRequestBuilder(getDownloadRequestBody(fileName));
            Response response = okHttpClient.newCall(request).execute();
            String json = ExpensifyUtil.convertCsvStringToJsonObject(response);
            List<ExpensifyDto> expensifyDto = objectMapper.readValue(json, new TypeReference<List<ExpensifyDto>>() {
            });
            Map<String, String> merchantSet = new HashMap<>();
            expensifyDto.stream().forEach(dto -> {
                String merchantDbId = merchantSet.get(dto.merchant);
                Merchant merchant = null;
                if (merchantDbId == null) {
                    merchant = merchantRepository.findByName(dto.merchant);
                    if (merchant == null) {
                        merchant = expensifyMapper.toMerchant(dto);
                        merchantRepository.save(merchant);
                        merchant = merchantRepository.findByName(dto.merchant);
                    }
                    merchantSet.put(dto.merchant, merchant.getId());
                    merchantDbId = merchant.getId();
                }
                saveOrUpdate(dto, merchantDbId);
            });
            syncEventsRepository.save(new SyncEvents());
        } catch (IOException e) {
            status = Boolean.FALSE;
            log.error("Exception while getting expenses info from expensify: ", e);
        }
        return status;
    }

    public void saveOrUpdate(ExpensifyDto dto, String merchantDbId) {
        try {
            Expense expenseToSave = expensifyMapper.toExpense(dto);
            expenseToSave.setMerchantId(merchantDbId);
            Query query = new Query();
            query.addCriteria(Criteria.where("transactionId").is(expenseToSave.getTransactionId()));
            Update update = new Update();
            update.set("transactionId", expenseToSave.getTransactionId());
            update.set("amount", expenseToSave.getAmount());
            update.set("merchantId", expenseToSave.getMerchantId());
            update.set("transactionDate", expenseToSave.getTransactionDate());

            mongoTemplate.upsert(query, update, Expense.class);

        } catch (Exception e) {
            log.error("Exception while saving expenses into DB", e);
        }

    }

}