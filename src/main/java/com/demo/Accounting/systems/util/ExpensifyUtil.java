package com.demo.Accounting.systems.util;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class ExpensifyUtil {

    /**
     * convert the csv string into json object.
     *
     * @param csvRows
     * @return String.
     */
    public static String convertCsvToJson(List<String> csvRows) {
        String json = csvToJson(csvRows);
        return json;
    }

    /**
     * convert the csv string into json object.
     *
     * @param response
     * @return String.
     */
    public static String convertCsvStringToJsonObject(Response response) {
        String[] csvString;
        try {
            csvString = response.body().string().trim().split("\\r?\\n");
            return ExpensifyUtil.convertCsvToJson(Arrays.asList(csvString));
        } catch (IOException e) {
            log.error("Exception while converting csv string to json object", e);
        }
        return null;
    }
    
    public static String csvToJson(List<String> csv) {
        //remove empty lines
        //this will affect permanently the list.
        //be careful if you want to use this list after executing this method
        csv.removeIf(e -> e.trim().isEmpty());
        //csv is empty or have declared only columns
        if (csv.size() <= 1) {
            return "[]";
        }
        //get first line = columns names
        String[] columns = csv.get(0).split(",");
        //get all rows
        StringBuilder json = new StringBuilder("[\n");
        csv.subList(1, csv.size()) //substring without first row(columns)
                .stream()
                .map(e -> e.split(","))
                .filter(e -> e.length == columns.length) //values size should match with columns size
                .forEach(row -> {
                    json.append("\t{\n");
                    for (int i = 0; i < columns.length; i++) {
                        json.append("\t\t\"")
                                .append(columns[i])
                                .append("\" : \"")
                                .append(row[i])
                                .append("\",\n"); //comma-1
                    }
                    //replace comma-1 with \n
                    json.replace(json.lastIndexOf(","), json.length(), "\n");
                    json.append("\t},"); //comma-2
                });
        //remove comma-2
        json.replace(json.lastIndexOf(","), json.length(), "");
        json.append("\n]");
        return json.toString();
    }

    public static String readTemplateFile() {
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        String ls = System.getProperty("line.separator");
        try {
            BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/templates/expensify_template.txt"));
            while (true) {
                if (!((line = reader.readLine()) != null)) {
                    break;
                }
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            // delete the last new line separator
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            reader.close();
        } catch (IOException e) {
            log.error("Exception while read expensify template file");
        }
        return stringBuilder.toString();
    }
}
